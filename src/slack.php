<?php

use Mnsr\LaravelSlack\SlackNotificationsClass;

if (!function_exists('slack_emergency')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_emergency($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        try {
            (new SlackNotificationsClass(['status' => 'emergency']))
                ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode());
        }
    }
}

if (!function_exists('slack_alert')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_alert($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        (new SlackNotificationsClass(['status' => 'alert']))
            ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
    }
}
if (!function_exists('slack_critical')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_critical($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        (new SlackNotificationsClass(['status' => 'critical']))
            ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
    }
}
if (!function_exists('slack_error')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_error($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        (new SlackNotificationsClass(['status' => 'error']))
            ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
    }
}
if (!function_exists('slack_warning')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_warning($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        (new SlackNotificationsClass(['status' => 'warning']))
            ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
    }
}
if (!function_exists('slack_notice')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_notice($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        (new SlackNotificationsClass(['status' => 'notice']))
            ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
    }
}
if (!function_exists('slack_info')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_info($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        (new SlackNotificationsClass(['status' => 'info']))
            ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
    }
}
if (!function_exists('slack_debug')) {
    /**
     * @param $msg
     * @param $title
     * @throws Exception
     */
    function slack_debug($msg, $title = '')
    {
        $send_message = '';
        if (gettype($msg) === 'array') {
            foreach ($msg as $key => $item) {
                $send_message .= $key . ':' . $item . "\n";
            }
        } else $send_message = $msg;

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        (new SlackNotificationsClass(['status' => 'debug']))
            ->sendNotification($caller['file'], $caller['line'], $send_message, $title);
    }
}

<?php

namespace Mnsr\LaravelSlack;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http;

/**
 *
 */
class SlackNotificationsClass
{

    /**
     * @var string
     */
    private string $proxy;

    /**
     * @var string
     */
    private string $channel;
    /**
     * @var string
     */
    private string $sluck_webhook_url;

    /**
     * @var string
     */
    private string $icon;
    /**
     * @var string
     */
    private string $color;
    /**
     * @var string
     */
    private string $username;

    /**
     * @var array|string[][]
     */
    private array $settings = [
        'emergency' => [
            'color' => '#9D0000',
            'icon' => ':sos:'
        ],
        'alert' => [
            'color' => '#1e33d0',
            'icon' => ':loudspeaker:'
        ],
        'critical' => [
            'color' => '#9D0000',
            'icon' => ':bomb:'
        ],
        'error' => [
            'color' => '#9D0000',
            'icon' => ':red_circle:'
        ],
        'warning' => [
            'color' => '#ffcc00',
            'icon' => ':heavy_exclamation_mark:'
        ],
        'notice' => [
            'color' => '#ebedd1',
            'icon' => ':bulb:'
        ],
        'info' => [
            'color' => '#d1ebed',
            'icon' => ':information_source:'
        ],
        'debug' => [
            'color' => '#d1edd3',
            'icon' => ':large_blue_circle:'
        ],
    ];

    /**
     * @param array $options [
     * 'status' => ['emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info', 'debug' => default]
     * 'channel' => $channel_name
     * ]
     *
     * @throws Exception
     */
    public function __construct(array $options = [])
    {
        if (config('logging.channels.slack.url') === null)
            throw new Exception('please set LOG_SLACK_WEBHOOK_URL value in .env !');

        $this->setChannel(env('SLACK_DEFAULT_CHANNEL', '#debug'));

        $this->proxy = env('PROXY_URL', '');
        $this->sluck_webhook_url = config('logging.channels.slack.url');

        if (isset($options['status']) && !empty($options['status']))
            $status = $options['status'];
        else $status = 'debug';

        if (isset($options['channel']) && !empty($options['channel'])) {
            if (!str_starts_with($options['channel'], '#'))
                $options['channel'] = '#' . $options['channel'];
            $this->setChannel($options['channel']);
        }

        $this->setSettings($status);
    }

    /**
     * @param string $channel
     */
    function setChannel(string $channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return string
     */
    private function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param $status
     */
    function setSettings($status)
    {
        $this->username = ucfirst($status);

        if (isset($this->settings[$status]))
            $this->setIcon($this->icon = $this->settings[$status]['icon']);
        else $this->setIcon($this->icon = $this->settings['info']['icon']);

        if (isset($this->settings[$status]))
            $this->setColor($this->settings[$status]['color']);
        else $this->setColor($this->settings['info']['color']);

    }

    /**
     * @param $icon
     */
    function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @param $color
     */
    function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @param $status
     */
    function setUsername($status)
    {
    }

    /**
     * @param $file_name
     * @param $line
     * @param $message
     * @param $title
     * @return array
     */
    private function buildMessage($file_name, $line, $message, $title)
    {
        return [
            'username' => $this->username,
            'icon_emoji' => $this->icon,
            'channel' => $this->getChannel(),
            "attachments" => [
                [
                    "mrkdwn_in" => ["text"],
                    "color" => $this->color,
                    "pretext" => $file_name . ' [L.' . $line . ']',
//                    "author_name" => "author_name",
//                    "author_link" => "http://flickr.com/bobby/",
//                    "author_icon" => "https://placeimg.com/16/16/people",
//                    "title" => "title",
//                    "title_link" => "https://api.slack.com/",
//                    "text" => $text,
//                    "text" => "Optional `text` that appears within the attachment",
                    "fields" => [
                        [
                            "title" => $title,
                            "value" => $message,
                            "short" => false
                        ],
//                        [
//                            "title" => "A short field's title",
//                            "value" => "A short field's value",
//                            "short" => true
//                        ],
//                        [
//                            "title" => "A second short field's title",
//                            "value" => "A second short field's value",
//                            "short" => true
//                        ]
                    ],
//                    "thumb_url" => "http://placekitten.com/g/200/200",
//                    "footer" => "Laravel Log",
//                    "footer_icon" => "https://laravel.com/img/favicon/favicon-16x16.png",
                    "ts" => Carbon::now()->timestamp
                ]
            ],
        ];
    }

    /**
     * @param string $file_name
     * @param $line
     * @param string $message
     * @param string $title
     * @throws Exception
     */
    function sendNotification(string $file_name, $line, string $message, $title = '')
    {
        $options = [];
        if ($this->proxy != '')
            $options = [
                'proxy' => $this->proxy
            ];
        $response = Http::withOptions($options)->post($this->sluck_webhook_url, $this->buildMessage($file_name, $line, $message, $title));
        $response->throw();
        if ($response->body() != 'ok')
            throw new Exception($response->body());
    }

}

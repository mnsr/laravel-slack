# Send Notifications to Slack channel

### Create slack webhook

Slack API webhook [official documentation](https://api.slack.com/messaging/webhooks)

### Add variables to .env

```dotenv
# Add if you have a proxy (Ex. http://192.168.1.1:3128)
PROXY_URL=

# Get from Slack Api
LOG_SLACK_WEBHOOK_URL=https://hooks.slack.com/services/xxx/yyy/zzz

# Your default slack channel
SLACK_DEFAULT_CHANNEL="my_channel"
```

Then clear config

```
php artisan config:cache
```

### Use functions directly in your code

```php
$msg // required parameter
$title // optional parameter

slack_emergency($msg, $title);
slack_alert($msg, $title);
slack_critical($msg, $title);
slack_error($msg, $title);
slack_warning($msg, $title);
slack_notice($msg, $title);
slack_info($msg, $title);
slack_debug($msg, $title);
```

Example

```php
try{
    // My Code xx ...
    slack_info('code xx finished successfully ..', 'my lovely title');
    // My Code yyy ...
}catch (Exception $exception){
    slack_error($exception->getMessage());
}
```

